# Maintainer: Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor: Karol Babioch <karol@babioch.de>

pkgname=python-pysnmp
_pyname=pysnmp
pkgver=5.0.23
pkgrel=1
pkgdesc='Open source and free implementation of v1/v2c/v3 SNMP engine'
url='https://github.com/pysnmp/pysnmp'
arch=('any')
license=('BSD')
depends=('python' 'python-pysmi' 'python-pyasn1' 'python-pycryptodomex')
makedepends=('python-sphinx' 'python-build' 'python-installer' 'python-poetry-core' 'python-wheel')
options=('!makeflags')
source=(https://github.com/pysnmp/pysnmp/archive/v${pkgver}/${_pyname}-v${pkgver}.tar.gz)
sha256sums=('b7145b2eed29a17d09584c8dd2938079d2d27fc758a4c3736deacd457fd05bc3')
sha512sums=('da6ff06e5f241c490d40e27d064ba629ffd00a1e8a6a89fc2601b9aa57d185513d8855a9a524a11cf91e0d79aa5de66a9535d070e3ab03fca7f97ea16b7c6714')

build() {
  cd ${_pyname}-${pkgver}
  export PYTHONPATH=.
  python -m build --wheel --no-isolation
  make -C docs text man
}

package() {
  cd ${_pyname}-${pkgver}
  python -m installer --destdir="${pkgdir}" dist/*.whl

  install -Dm 644 README.md -t "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r docs/build/text "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r examples -t "${pkgdir}/usr/share/doc/${pkgname}"

  install -Dm 644 docs/build/man/${_pyname}.1 "${pkgdir}/usr/share/man/man1/${_pyname}.1"
  install -Dm 644 docs/build/man/${_pyname}.1 "${pkgdir}/usr/share/man/man1/${pkgname}.1"
  install -Dm 644 LICENSE.rst -t "${pkgdir}/usr/share/licenses/${pkgname}"
}

# vim: ts=2 sw=2 et:
